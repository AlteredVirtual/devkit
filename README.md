# Windows 10 devkit
### Table of Contents
 1. [Core Dependencies](#core-dependencies)
 2. [Visual Studio Code](#visual-studio-code)
 3. [Git & Github & Gitlab](#git-github-gitlab)
 4. [Theme](#theme)
 5. [Terminal & Shell](#terminal--shell)
 6. [Node.js](#nodejs)
 7. [Docker](#docker)
 


## Core Dependencies
 * `sudo apt install python3-dev`
 * `sudo apt install libssl-dev`
 * `sudo apt install curl`
 * `sudo apt install cmake`

<br>



## Git & Github & Gitlab
#### Install Git & Configure SSH
 1. `sudo apt install git`
 2. [Generate SSH Key](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
 3. [Add SSH Key to Github or Gitlab Account ](https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/)

<br>

## Theme
#### Fonts
 1. [Input](http://input.fontbureau.com/download/index.html?size=15&language=python&theme=monokai&family=InputMono&width=300&weight=400&line-height=1.3&a=ss&g=ss&i=serifs_round&l=serifs_round&zero=slash&asterisk=height&braces=0&preset=default&customize=please) *(Dowonload & Install)*
 2. [Hack](https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip) *(Dowonload & Install)*
 3. Source Code Pro
    * `git clone --depth 1 --branch release https://github.com/adobe-fonts/source-code-pro.git ~/.fonts/adobe-fonts/source-code-pro`
    * `fc-cache -f -v ~/.fonts/adobe-fonts/source-code-pro`


### Node.js
#### Install Node.js
 ```sh
 # install nodejs 11
 curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
 sudo apt install -y nodejs
 ```

#### Change the Location of Global Packages
 1. `cd ~ && mkdir .node_modules_global`
 2. `npm config set prefix=$HOME/.node_modules_global`
 3. `npm install npm@latest -g`

#### Configure Defaults
 ```sh
 npm config set init.author.name <name>
 npm config set init.author.email <email>
 ```
<br>

## Terminal & Shell

#### [Zsh](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH)
 1. Install Zsh
    * `brew install zsh`
    * `sudo sh -c "echo $(which zsh) >> /etc/shells" && chsh -s $(which zsh)`
    * *Login*
    * `echo $SHELL`
 2. [Oh My Zsh](https://github.com/robbyrussell/oh-my-zsh)
    * `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
 3. [Spaceship Prompt](https://github.com/denysdovhan/spaceship-prompt) | [Spaceship docs](https://denysdovhan.com/spaceship-prompt/docs/Options.html)
    * `git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"`
    * `ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"`
 4. [Zsh Autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
    * `git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions`
    * `echo "bindkey '^ ' autosuggest-accept" >> $ZSH_CUSTOM/autosuggestion-settings.zsh`
    * `source $ZSH_CUSTOM/autosuggestion-settings.zsh`
 5. [Zsh Syntax Highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
    * `git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting`
 6. Configure `.zshrc`
    * `curl -o ~/.zshrc https://gitlab.com/TheYkk/devkit/raw/master/.zshrc`
    * `source ~/.zshrc`
<br>

## Visual Studio Code
 1. [Download the .deb package](https://code.visualstudio.com/docs/?dv=linux64_deb) 
 2. Download Extension
    * Settings Sync
 3. Configure own settings
 4. Copy the [settings file](https://gitlab.com/AlteredVirtual/devkit/-/blob/master/settings.json) contents into settings.json
<br>

## Docker
  ### Docker install
  *  Docker [install](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository)
   ```sh
   #set for user
   sudo usermod -aG docker $(whoami)
  ```

  ### Manage docker
  1. Kitematic [download](https://github.com/docker/kitematic/releases)
  2. DockStation [install](https://dockstation.io/)
  3. Portainer [install](https://www.portainer.io/installation/)


## Misc

* ClickUP (trello alternetive) [site](https://clickup.com/)
* [Skm](https://gitlab.com/TheYkk/skm)
* [Txeh](https://github.com/txn2/txeh)

### Recovery
 [Site](https://askubuntu.com/questions/776146/reinstalling-critical-files-in-recovery-mode)
