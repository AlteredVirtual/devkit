# ? Core
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew  update
brew install curl cmake 

# ? Install Git
brew install git

# ? Install Zsh
brew install zsh

# ? Oh My Zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# ? Spaceship Prompt | Spaceship docs
git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"

# ? Zsh Autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
echo "bindkey '^ ' autosuggest-accept" >> $ZSH_CUSTOM/autosuggestion-settings.zsh
source $ZSH_CUSTOM/autosuggestion-settings.zsh

# ? Zsh Syntax Highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# ? Install fzf
brew install fzf

# ? Configure .zshrc
curl -o ~/.zshrc https://gitlab.com/TheYkk/devkit/raw/master/.zshrc
source ~/.zshrc

# ? Install Docker
curl https://get.docker.com | sh
curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# ? Install Nodejs 12
brew install node 

# ? Install yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | brew-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/brew/sources.list.d/yarn.list
brew update && brew install  yarn

# ? Yarn packages
yarn global add nodemon 
yarn global add pino-pretty
yarn global add @vue/cli 
yarn global add eslint

# ? Install VS CODE
brew cask install visual-studio-code

# ? Install Postman
brew cask install postman

# ? Install Insomnia
brew cask install insomnia

# ? Install Kubectl
brew install kubectl

# ? Install Kubectx
brew install kubectx

# ? Install Kube-alias
curl https://raw.githubusercontent.com/ahmetb/kubectl-alias/master/.kubectl_aliases > ~/.kubectl_aliases

# ? Install Helm
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

# ? Install Golang
brew install go

# ? Install Git flow
brew unlink git-flow
brew install git-flow-avh

# ? Install k3sup
curl -sLS https://get.k3sup.dev | sh
install k3sup /usr/local/bin/

# ? Install Brave
brew install brave

# ? Install fonts
brew tap homebrew/cask-fonts
brew cask install font-fira-code
